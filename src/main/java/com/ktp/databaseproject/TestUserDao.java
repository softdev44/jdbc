/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.databaseproject;

import com.ktp.databaseproject.dao.UserDao;
import com.ktp.databaseproject.helper.DatabaseHelper;
import com.ktp.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
//        System.out.println(userDao.getAll());
//        System.out.println(userDao.get(1));
//        User user = new User(3,"namin", "password", 2, "fm");
//        userDao.save(user);
//        System.out.println(userDao.getAll());
//        User user1 = userDao.get(2);
//        System.out.println(user1);
//
//        user1.setGender("M");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//
//        //userDao.delete(userDao.get(4));
//        System.out.println(userDao.getAll());
//        for (User u : userDao.getAll()) {
//            System.out.println(u);
//        }

        for (User u : userDao.getAllOrderBy("user_name", "asc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }

}
