/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.databaseproject.dao;

import com.ktp.databaseproject.helper.DatabaseHelper;
import com.ktp.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class UserDao  implements Dao<User> {

     @Override
    public User get(int id) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                user = User.fromRS(rs);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }
    public User getByName(String name) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user WHERE user_name=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                user = User.fromRS(rs);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }
    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    @Override
    public List<User> getAllOrderBy(String name, String order) {
        ArrayList<User> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user ORDER BY "+name+" " + order;
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setName(rs.getString("user_name"));
                user.setGender(rs.getString("user_gender"));
                user.setPassword(rs.getString("user_password"));
                user.setRole(rs.getInt("user_role"));
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    @Override
    public User save(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO user (user_name, user_password, user_role, user_gender)"
                + "VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3, obj.getRole());
            stmt.setString(4, obj.getGender());
            stmt.executeUpdate();
            obj.setId(DatabaseHelper.getInsertedId(stmt));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE user"
                + " SET user_name = ?, user_password = ?, user_role = ?, user_gender = ?"
                + " WHERE user_id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3, obj.getRole());
            stmt.setString(4, obj.getGender());
            stmt.setInt(5, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
}
