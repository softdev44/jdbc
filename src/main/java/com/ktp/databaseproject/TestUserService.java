/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.databaseproject;

import com.ktp.databaseproject.model.User;
import com.ktp.databaseproject.service.UserService;

/**
 *
 * @author acer
 */
public class TestUserService {
     public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("minnan","password");
        if (user!=null){
            System.out.println("Welcome User : "+user.getName());
        }else {
            System.out.println("Error");
        }
    }
}
