/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author acer
 */
public class DeleteDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connecttion to SQLite has been esteblish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        //select
        String sql = "DELETE FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,3);
            int status = stmt.executeUpdate();
            // ResultSet rs = stmt.executeQuery(sql);

//            while (rs.next()) {
//                System.out.println(rs.getInt("category_id") + " " + rs.getString("category_name"));
//            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());    
        }

        //close
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
