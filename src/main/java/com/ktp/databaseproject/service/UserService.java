/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.databaseproject.service;

import com.ktp.databaseproject.dao.UserDao;
import com.ktp.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class UserService {
     public User login (String name, String password){
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user != null && user.getPassword().equals(password)){
            return user;
        }
        
        return null;
    }
}
